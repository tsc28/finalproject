# Amazon 31
Amazon 31 is an online shopping site that allows users to browse, buy/sell, and review products.

## Installation
We recommend using PyCharm, an IDE that has a Flask command-line interface.

To run the code locally, make sure the latest Python version (Python 3.7) is installed on your machine.
Additionally, install SQLite version 3.32.3 for your OS (either Mac or Windows) here: https://www.sqlite.org/download.html

In PyCharm, set your Python Interpreter to Python 3.7 in Preferences > Project > Python Interpreter.
When configuring your Python Interpreter, check specifically for the following packages:

- FLask 1.1.2
- Flask-Mail 0.9.1
- pandas 1.1.4

If any packages are missing, click the + button in Preferences > Project > Python Interpreter to install them.

## Running Amazon 31 Locally
In PyCharm, run _createdatabase.py_, then _importdata.py_, then _app.py_ by clicking 'Run' in the top-right corner.
Alternatively, you can view the 'Run' menu options in your top navigation bar. You should be using the Python Interpreter 
that you just configured.

_createdatabase.py_ initializes all tables in big_amazon.db and _importdata.py_ populates those tables with data.

After running _app.py_, PyCharm will provide a link to a development server in the command-line interface. The link
will open in your browser.

## Deployment
We deployed our project on Heroku (see requirements.txt for additional details). We included a link to our deployed app 
in our final report. You can view it here: https://amazon31.herokuapp.com/

Heroku automatically resets the database when it shuts down or powers down, so you need to signup a new account each time you access the app after a long period of time. Heroku's automatically powers down after 30 minutes of inactivity. 

Default user account:
username: lydiajlin, password: 123

Default seller account:
username: seller1, password:123