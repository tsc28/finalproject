from flask import Flask, render_template, url_for, request
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
import random
from databaseutil import *
import pandas as pd

random.seed(30)

def initialize_all_tables(conn):
    #user_type 0 is buyer 1 is seller
    create_user_table = """CREATE TABLE User
                        (user_ID INTEGER NOT NULL PRIMARY KEY,
                        username TEXT NOT NULL, 
                        first_name TEXT NOT NULL,
                        last_name TEXT NOT NULL,
                        email TEXT NOT NULL,
                        password TEXT NOT NULL,
                        balance MONEY NOT NULL,
                        user_type BIT NOT NULL,
                        street_address TEXT NOT NULL,
                        city TEXT NOT NULL,
                        postalcode TEXT NOT NULL,
                        state TEXT NOT NULL);"""

    create_products_table = """CREATE TABLE Products
                        (product_ID INTEGER NOT NULL PRIMARY KEY,
                        name TEXT NOT NULL, 
                        brand TEXT NOT NULL, 
                        category TEXT NOT NULL, 
                        price MONEY NOT NULL,
                        feature TEXT,
                        image TEXT, 
                        description TEXT,
                        inventory SMALLINT NOT NULL);"""

    create_prereview_table = """CREATE TABLE PreReviewsRatings
                            (prereview_ID INTEGER NOT NULL,
                            rating SMALLINT NOT NULL, 
                            review TEXT,
                            summary TEXT,
                            vote SMALLINT DEFAULT 0,
                            PRIMARY KEY(prereview_ID));"""

    create_review_table = """CREATE TABLE ReviewsRatings
                        (review_ID INTEGER NOT NULL,
                        user_ID INTEGER NOT NULL,
                        product_ID INTEGER NOT NULL,
                        date_time DATETIME NOT NULL default current_timestamp,
                        username TEXT,
                        rating SMALLINT NOT NULL, 
                        review TEXT,
                        summary TEXT,
                        vote SMALLINT DEFAULT 0,
                        PRIMARY KEY(review_ID),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(product_ID) REFERENCES Product(product_ID));"""

    create_cart_table = """CREATE TABLE Cart
                                (cart_ID INTEGER NOT NULL,
                                user_ID INTEGER NOT NULL,
                                cart_status INTEGER NOT NULL,
                                PRIMARY KEY(cart_ID),
                                FOREIGN KEY(user_ID) REFERENCES User(user_ID));"""

    create_sells_table = """CREATE TABLE Sells
                            (product_ID INTEGER NOT NULL PRIMARY KEY,
                            user_ID INTEGER NOT NULL,
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(product_ID) REFERENCES Product(product_ID));"""

    create_orders_table = """CREATE TABLE Orders
                                (item_order_ID INTEGER NOT NULL,
                                cart_ID INTEGER NOT NULL,
                                product_ID INTEGER NOT NULL,
                                user_ID INTEGER NOT NULL,
                                quantity INTEGER NOT NULL,
                                PRIMARY KEY(item_order_ID),
                                FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                                FOREIGN KEY(cart_ID) REFERENCES Cart(Cart_ID),
                                FOREIGN KEY(product_ID) REFERENCES Products(product_ID));"""

    create_purchases_table = """CREATE TABLE Purchases
                                (user_ID INTEGER NOT NULL,
                                cart_ID INTEGER NOT NULL,
                                amount REAL NOT NULL,
                                date_time NOT NULL default current_timestamp,
                                PRIMARY KEY(cart_ID),
                                FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                                FOREIGN KEY(cart_ID) REFERENCES Cart(Cart_ID));"""

    create_table(conn, create_user_table)
    create_table(conn, create_products_table)
    create_table(conn, create_review_table)
    create_table(conn, create_cart_table)
    create_table(conn, create_sells_table)
    create_table(conn, create_orders_table)
    create_table(conn, create_purchases_table)


def importdata(conn):
    c = conn.cursor()

    print('USER')
    read_clients = pd.read_csv(r'dataclean/csv/user.csv',
                               usecols = ['username', 'first_name', 'last_name', 'email', 'password',
                                           'balance', 'user_type', 'street_address', 'city', 'postalcode', 'state'])
    read_clients.to_sql('User', conn, if_exists='append', index=False)

    print('PRODUCT')
    read_clients = pd.read_csv(r'dataclean/csv/product.csv',
                               usecols = ['name' ,'brand', 'category', 'price', 'feature', 'image', 'description', 'inventory'])
    read_clients.to_sql('Products', conn, if_exists='append', index=False)

    print('PREREVIEW')
    read_clients = pd.read_csv(r'dataclean/csv/review.csv',
                               usecols = ['rating' ,'review', 'summary', 'vote'])
    read_clients.to_sql('PreReviewsRatings', conn, if_exists='append', index=False)

    #print('SELLS')
    #read_clients = pd.read_csv(r'dataclean/csv/sells.csv')
    #read_clients.to_sql('Sells', conn, if_exists='append', index=False)


def setup_purchase_related(conn):
    try:
        sql = """ SELECT user_ID FROM User WHERE user_type = 0;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID, price FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        order_count = 100
        for count in range(0, order_count):
            uid_index = random.randint(0, len(user_rows) - 1)
            uid = user_rows[uid_index][0]

            cart = (uid, 0)
            sql = """INSERT INTO Cart(user_id, cart_status) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, cart)
            cid = cur.lastrowid

            total_cost = 0
            num_order_item = random.randint(0, 5)
            for n in range(0, num_order_item):
                product_index = random.randint(0, len(product_rows) - 1)
                quantity = random.randint(1, 30)
                pid = product_rows[product_index][0]
                total_cost = total_cost + (quantity * product_rows[product_index][1])
                order = (cid, pid, uid, quantity)
                sql = """INSERT INTO Orders(cart_ID, product_ID, user_ID, quantity) VALUES (?,?,?,?);"""
                cur = conn.cursor()
                cur.execute(sql, order)
                # item_order_id = insert(conn, 'Orders(cart_ID, product_ID, user_ID, quantity)', order)

            purchase = (uid, cid, total_cost)
            sql = """INSERT INTO Purchases(user_ID, cart_ID, amount) VALUES (?,?,?);"""
            cur = conn.cursor()
            cur.execute(sql, purchase)
            # insert(conn, 'Purchases1(user_ID, cart_ID, amount)', purchase)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()
    finally:
        print('CART\nORDERS\nPURCHASES')


def active_cart(conn):
    try:
        sql = """ SELECT user_ID FROM User WHERE user_type = 0;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID, price FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        active_cart_count = len(user_rows)
        for uid_index in range(0, active_cart_count):
            uid = user_rows[uid_index][0]

            cart = (uid, 1)
            sql = """INSERT INTO Cart(user_id, cart_status) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, cart)
            cid = cur.lastrowid

            total_cost = 0
            num_order_item = random.randint(0, 5)

            for n in range(0, num_order_item):
                product_index = random.randint(0, len(product_rows)-1)
                quantity = random.randint(1, 30)
                pid = product_rows[product_index][0]
                total_cost = total_cost + (quantity * product_rows[product_index][1])
                order = (cid, pid, uid, quantity)
                sql = """INSERT INTO Orders(cart_ID, product_ID, user_ID, quantity) VALUES (?,?,?,?);"""
                cur = conn.cursor()
                cur.execute(sql, order)
                # item_order_id = insert(conn, 'Orders1(cart_ID, product_ID, user_ID, quantity)', order)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()


def add_sellers(conn):
    try:
        sql = """ SELECT user_ID FROM User;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID, price FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        for pid_index in range(0, len(product_rows)):
            pid = product_rows[pid_index][0]

            user_index = random.randint(0, int(len(user_rows)/2))
            uid = user_rows[user_index][0]
            sql = """UPDATE User
                SET user_type = 1
                WHERE user_ID = ?;"""
            cur = conn.cursor()
            cur.execute(sql, (uid,))

            sells = (pid, uid)
            sql = """INSERT INTO Sells(product_ID, user_ID) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, sells)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()
    finally:
        print('SELLS')


def add_reviews(conn):
    try:
        sql = """ SELECT user_ID, username FROM User WHERE user_type = 0;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        sql = """ SELECT * FROM PreReviewsRatings;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        prereview_rows = cur.fetchall()

        for review_index in range(0, len(prereview_rows)):
            #rid = prereview_rows[review_index]["prereview_ID"]

            user_index = random.randint(0, len(user_rows)-1)
            uid = user_rows[user_index][0]
            username = user_rows[user_index][1]

            product_index = random.randint(0, len(product_rows) - 1)
            pid = product_rows[product_index][0]

            review = (uid, pid, username, prereview_rows[review_index]["rating"],
                      prereview_rows[review_index]["review"], prereview_rows[review_index]["summary"],
                      prereview_rows[review_index]["vote"])
            sql = """INSERT INTO ReviewsRatings(user_ID, product_ID, username, rating, review, summary, vote) VALUES (?,?,?,?,?,?);"""
            cur = conn.cursor()
            cur.execute(sql, review)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()
    finally:
        print('REVIEWS')


# initializes entire database and preloaded values
def initialize_db(file):
    initiated = False
    if os.path.exists(file):
        initiated = True

    if not initiated:
        # creates database local file
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        database = os.path.join(THIS_FOLDER, file)
    else:
        print("Database already initialized")

    conn = create_connection(file)
    initialize_all_tables(conn)
    importdata(conn)
    add_sellers(conn)
    add_reviews(conn)
    setup_purchase_related(conn)
    active_cart(conn)
    conn.close()


if __name__ == "__main__":
    reset('new_amazon.db')
    initialize_db('new_amazon.db')