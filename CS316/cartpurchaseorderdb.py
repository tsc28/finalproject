#File for all general database functions, makes initial tables - Lydia
from flask import Flask, render_template, url_for, request
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
import random
from databaseutil import *

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
#database = os.path.join(THIS_FOLDER, 'database.py')
database = os.path.join(THIS_FOLDER, 'big_amazon.db')

def create_cart_order_purchase():
    conn = create_connection(database)

    create_cart_table = """CREATE TABLE Cart1
                            (cart_ID INTEGER NOT NULL,
                            user_ID TEXT NOT NULL,
                            cart_status INTEGER NOT NULL,
                            PRIMARY KEY(cart_ID),
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID));"""

    create_orders_table = """CREATE TABLE Orders1
                            (item_order_ID INTEGER NOT NULL,
                            cart_ID INTEGER NOT NULL,
                            product_ID TEXT NOT NULL,
                            user_ID TEXT NOT NULL,
                            quantity INTEGER NOT NULL,
                            PRIMARY KEY(item_order_ID),
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(cart_ID) REFERENCES Cart1(Cart1_ID),
                            FOREIGN KEY(product_ID) REFERENCES Products(product_ID));"""

    create_purchases_table = """CREATE TABLE Purchases1
                            (user_ID TEXT NOT NULL,
                            cart_ID INTEGER NOT NULL,
                            amount REAL NOT NULL,
                            date_time NOT NULL default current_timestamp,
                            PRIMARY KEY(cart_ID),
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(cart_ID) REFERENCES Cart1(Cart1_ID));"""
    try:
        create_table(conn, create_cart_table)
        create_table(conn, create_orders_table)
        create_table(conn, create_purchases_table)

        sql = """ SELECT user_ID FROM User WHERE user_type = 0;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID, price FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        order_count = 1000
        for count in range(0,order_count):
            uid_index = random.randint(0, len(user_rows)-1)
            uid = user_rows[uid_index][0]

            cart = (uid, 0)
            sql = """INSERT INTO Cart1(user_id, cart_status) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, cart)
            cid = cur.lastrowid

            total_cost = 0
            num_order_item = random.randint(0,5)
            for n in range (0, num_order_item):
                product_index = random.randint(0, len(product_rows)-1)
                quantity = random.randint(1,30)
                pid = product_rows[product_index][0]
                total_cost = total_cost + (quantity*product_rows[product_index][1])
                order = (cid, pid, uid, quantity)
                sql = """INSERT INTO Orders1(cart_ID, product_ID, user_ID, quantity) VALUES (?,?,?,?);"""
                cur = conn.cursor()
                cur.execute(sql, order)
                #item_order_id = insert(conn, 'Orders11(cart_ID, product_ID, user_ID, quantity)', order)

            purchase = (uid, cid, total_cost)
            sql = """INSERT INTO Purchases1(user_ID, cart_ID, amount) VALUES (?,?,?);"""
            cur = conn.cursor()
            cur.execute(sql, purchase)
            #insert(conn, 'Purchases11(user_ID, cart_ID, amount)', purchase)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()
    finally:
        conn.close()

def active_cart():
    conn = create_connection(database)

    try:
        sql = """ SELECT user_ID FROM User WHERE user_type = 0;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        user_rows = cur.fetchall()

        sql = """ SELECT product_ID, price FROM Products;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        product_rows = cur.fetchall()

        order_count = 30
        for uid_index in range(0, order_count):
            uid = user_rows[uid_index][0]

            cart = (uid, 1)
            sql = """INSERT INTO Cart1(user_id, cart_status) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, cart)
            cid = cur.lastrowid

            total_cost = 0
            num_order_item = random.randint(0, 5)

            for n in range(0, num_order_item):
                product_index = random.randint(0, len(product_rows)-1)
                quantity = random.randint(1, 30)
                pid = product_rows[product_index][0]
                total_cost = total_cost + (quantity * product_rows[product_index][1])
                order = (cid, pid, uid, quantity)
                sql = """INSERT INTO Orders1(cart_ID, product_ID, user_ID, quantity) VALUES (?,?,?,?);"""
                cur = conn.cursor()
                cur.execute(sql, order)
                # item_order_id = insert(conn, 'Orders11(cart_ID, product_ID, user_ID, quantity)', order)

            conn.commit()

    except Error as e:
        msg = e
        conn.rollback()
    finally:
        conn.close()

if __name__ == "__main__":
    create_cart_order_purchase()
    active_cart()