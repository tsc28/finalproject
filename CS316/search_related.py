from flask import Flask, render_template, url_for, request, redirect, session, flash
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
from database import *
from databaseutil import *
from purchase_related import *
from app import *

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
database = os.path.join(THIS_FOLDER, 'big_amazon.db')

app = Flask(__name__)

def homepage():
    msg = "msg"

    try:
        database = r"./big_amazon.db"
        conn = create_connection(database)
        sql = '''SELECT * FROM Products
                    ORDER BY random() LIMIT 3'''
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        conn.commit()
        msg = "db search success"
        cur.close()
    except:
        conn.rollback()
        msg = "db search fail"
        rows = []
    return render_template('index.html', msg=msg, rows=rows)

def search_searchThis():
    msg = "msg"
    rows=[]
    if request.method == "POST":
        try:
            search_query = request.form["search_query"]
            category_selected = request.form["categorydropdown"]
            category_query = ''
            if (category_selected != 'Categories'):
                category_query = " AND category = '" + category_selected + "' "

            database = r"./big_amazon.db"
            conn = create_connection(database)


            sql = """SELECT * FROM Products 
                WHERE (name LIKE '%{0}%' OR brand LIKE '%{0}%') {1}
                ORDER BY (SELECT AVG(rating) 
                    FROM ReviewsRatings 
                    WHERE ReviewsRatings.product_ID = Products.product_ID 
                    GROUP BY ReviewsRatings.product_ID)""".format(search_query, category_query)
            # returns partial match on name, brand
            conn.row_factory=sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            conn.commit()
            msg = "db search success"
            cur.close()
        except:
            conn.rollback()
            msg = "db search fail"
            rows = []
        return render_template('searchresult.html', msg = msg, rows = rows)

def search_showItem():
    myPID = request.args.get('product_ID')
    if request.method == "GET":
        msg = ""
        rows = []
        reviewrows = []
        similarrows = []

        try:
            # creates database and connects
            database = r"./big_amazon.db"
            conn = create_connection(database)

            sql = """SELECT *
                    FROM Products
                    WHERE product_ID = ?;"""

            reviewsql = """SELECT * FROM ReviewsRatings WHERE ReviewsRatings.product_ID = ?;"""

            similaritems = """SELECT * FROM Products
                            WHERE Products.brand = 
                            (SELECT brand FROM Products WHERE product_ID = ?)
                            ORDER BY random() LIMIT 5;"""

            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (myPID,))
            rows = cur.fetchall()
            conn.commit()

            cur.execute(reviewsql, (myPID,))
            reviewrows = cur.fetchall()
            conn.commit()

            cur.execute(similaritems, (myPID,))
            similarrows = cur.fetchall()
            conn.commit()
            cur.close()

        except:
            conn.rollback()
            msg = "fail"
            rows = []
            reviewrows = []
            similarrows = []

        return render_template('itempage.html', msg=msg, reviewrows=reviewrows, rows=rows, similarrows=similarrows)