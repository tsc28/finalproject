import sqlite3
import pandas as pd
from pandas import DataFrame


def importdata():
    conn = sqlite3.connect('big_amazon.db')
    c = conn.cursor()

    print('USER')
    read_clients = pd.read_csv(r'dataclean/csv/user.csv')
    read_clients.to_sql('User', conn, if_exists='append', index=False)

    print('PRODUCT')
    read_clients = pd.read_csv(r'dataclean/csv/product.csv')
    read_clients.to_sql('Products', conn, if_exists='append', index=False)

    print('REVIEW')
    read_clients = pd.read_csv(r'dataclean/csv/review.csv')
    read_clients.to_sql('ReviewsRatings', conn, if_exists='append', index=False)

    print('SELLS')
    read_clients = pd.read_csv(r'dataclean/csv/sells.csv')
    read_clients.to_sql('Sells', conn, if_exists='append', index=False)

    print('CART')
    read_clients = pd.read_csv(r'dataclean/csv/cart.csv')
    read_clients.to_sql('Cart', conn, if_exists='append', index=False)

    print('ORDERS')
    read_clients = pd.read_csv(r'dataclean/csv/orders.csv')
    read_clients.to_sql('Orders', conn, if_exists='append', index=False)

    print('PURCHASES')
    read_clients = pd.read_csv(r'dataclean/csv/purchases.csv')
    read_clients.to_sql('Purchases', conn, if_exists='append', index=False)

if __name__ == "__main__":
    importdata()
