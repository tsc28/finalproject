import pandas as pd
import numpy as np
import json
import gzip
import glob
import os
import string
import random

#TODO find user info
#TODO work on solution combining different categories


def filetodf(filename, size):
    data = []
    with gzip.open(filename) as f:
        count = 0
        for l in f:
            data.append(json.loads(l.strip()))
            if count == size:
                break
            count += 1

    df = pd.DataFrame.from_dict(data)
    printinfo(df)
    return df


def cleanmetadata(df, cat):
    selectdf = df.loc[(df['asin'] != '') & (df['title'] != '') & (df['price'] != '') & (df['brand'] != '')
                     & (df.price.str.len() < 10) & (df.description.str.len() > 0)
                      & (df.main_cat.str.len() > 0)]
    selectdf = selectdf[selectdf.columns.drop(list(selectdf.filter(regex='details')))]
    cleandf = selectdf.drop(['tech1', 'fit', 'also_buy', 'tech2', 'also_view', 'category',
                             'similar_item', 'date', 'rank', 'main_cat'], axis=1)\
        .drop_duplicates(subset='asin', keep='first').head(2000)
    np.random.seed(42)
    cleandf['inventory'] = np.random.randint(0, 3000, cleandf.shape[0])
    cleandf['price'] = cleandf['price'].str.replace(',', '')
    cleandf['price'] = cleandf['price'].str.replace('$', '')
    cleandf['price'] = cleandf['price'].astype('float')
    cleandf['category'] = cat

    cleandf = cleandf.rename(columns={'asin': 'product_ID', 'title': 'name'})
    cleandf = cleandf[['product_ID', 'name', 'brand', 'category', 'price', 'feature',
                       'image', 'description', 'inventory']]

    printinfo(cleandf)
    return cleandf


def combinemetadata(sample):
    print('--Appliances--')
    appliances_df = filetodf('rawdata/meta_Appliances.json.gz', sample)
    appliances_df = cleanmetadata(appliances_df, 'Appliances')
    print('--Books--')
    books_df = filetodf('rawdata/meta_Books.json.gz', sample)
    #testcsv = books_df.to_csv(r'csv\test.csv', index=None, header=True)
    books_df = cleanmetadata(books_df, 'Books')
    print('--Home and Kitchen--')
    home_df = filetodf('rawdata/meta_Home_and_Kitchen.json.gz', sample)
    home_df = cleanmetadata(home_df, 'Home and Kitchen')
    print('--Electronics--')
    electronics_df = filetodf('rawdata/meta_Electronics.json.gz', sample)
    electronics_df = cleanmetadata(electronics_df, 'Electronics')
    print('--Clothing--')
    clothing_df = filetodf('rawdata/meta_Clothing_Shoes_and_Jewelry.json.gz', sample)
    clothing_df = cleanmetadata(clothing_df, 'Clothing Shoes and Jewelry')
    metadatadf = pd.concat([appliances_df, books_df, home_df, electronics_df, clothing_df], axis=0)
    print('--Final--')
    printinfo(metadatadf)
    return metadatadf


def cleanreview(rawreviewdf, cleanmetadf, userdf):
    cleandf = rawreviewdf.loc[rawreviewdf['asin'].isin(cleanmetadf['product_ID'])]
    cleandf = cleandf.drop(['verified', 'unixReviewTime', 'image', 'reviewerID', 'reviewerName'], axis=1)

    cleandf = cleandf.rename(columns={'asin': 'product_ID', 'overall': 'rating', 'reviewTime': 'date_time',
                                      'reviewText': 'review'})

    buyerdf = userdf.loc[(userdf['user_type'] == 0)]
    buyerdf = buyerdf.sample(n=cleandf.shape[0], replace=True)
    cleandf['user_ID'] = buyerdf['user_ID'].to_numpy()
    cleandf['username'] = buyerdf['username'].to_numpy()

    cleandf = cleandf[['user_ID', 'product_ID', 'date_time', 'username', 'rating', 'review', 'summary', 'vote']]
    cleandf = cleandf.groupby('product_ID').head(10).dropna(subset=['user_ID', 'product_ID', 'date_time', 'rating'])\
        .drop_duplicates(subset=['user_ID', 'product_ID', 'date_time'], keep='first')
    cleandf['rating'] = cleandf['rating'].astype('int')
    cleandf['vote'] = cleandf['vote'].str.replace(',', '')
    cleandf['vote'] = cleandf['vote'].fillna(0).astype('int')

    start = pd.to_datetime('2018-01-01')
    end = pd.to_datetime('2020-01-01')
    cleandf['date_time'] = datetimegenerator(start, end, cleandf.shape[0])

    printinfo(cleandf)
    return cleandf


def combinereview(metadatadf, userdf, sample):
    print('--Appliances--')
    appliances_df = filetodf('rawdata/Appliances.json.gz', sample)
    appliances_df = cleanreview(appliances_df, metadatadf, userdf)
    print('--Books--')
    books_df = filetodf('rawdata/Books.json.gz', sample)
    books_df = cleanreview(books_df, metadatadf, userdf)
    print('--Home and Kitchen--')
    home_df = filetodf('rawdata/Home_and_Kitchen.json.gz', sample)
    home_df = cleanreview(home_df, metadatadf, userdf)
    print('--Electronics--')
    electronics_df = filetodf('rawdata/Electronics.json.gz', sample)
    electronics_df = cleanreview(electronics_df, metadatadf, userdf)
    print('--Clothing--')
    clothing_df = filetodf('rawdata/Clothing_Shoes_and_Jewelry.json.gz', sample)
    clothing_df = cleanreview(clothing_df, metadatadf, userdf)
    print('--Final--')
    reviewdf = pd.concat([appliances_df, books_df, home_df, electronics_df, clothing_df], axis=0)
    printinfo(reviewdf)
    return reviewdf


def combineuser(path, bsswitch):
    all_files = glob.glob(os.path.join(path, "*.csv"))

    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    frame = pd.concat(li, axis=0, ignore_index=True)
    frame = frame.drop_duplicates(subset=['username'], keep='first').drop_duplicates(subset=['email'], keep='first')
    frame['user_type'] = bsswitch
    printinfo(frame)
    return frame


def createuser(bdf, sdf):
    num_rows = 5000
    datab = np.array([idgenerator(9) for i in range(num_rows)])
    datas = np.array([idgenerator(9) for i in range(num_rows)])
    bdf['user_ID'] = pd.DataFrame(datab)
    sdf['user_ID'] = pd.DataFrame(datas)
    userdf = pd.concat([bdf, sdf], axis=0)
    userdf = userdf.drop_duplicates(subset=['user_ID'], keep='first')

    datap = np.array([pwdgenerator() for i in range(num_rows*2)])
    userdf['password'] = pd.DataFrame(datap)
    userdf['user_ID'] = userdf['user_ID'].astype('int64')

    userdf = userdf[['user_ID', 'username', 'first_name', 'last_name', 'email', 'password', 'balance', 'user_type',
                     'street_address', 'city', 'postalcode', 'state']]

    printinfo(userdf)
    return userdf


def joinProdRev(product_df, review_df):
    productids = product_df['product_ID'].tolist()
    pdf = product_df
    rdf = review_df
    for i in productids:
        prodid = idgenerator(9)
        pdf.loc[pdf['product_ID'] == i, ['product_ID']] = prodid
        rdf.loc[rdf['product_ID'] == i, ['product_ID']] = prodid

    pdf['product_ID'] = pdf['product_ID'].astype('int64')
    rdf['product_ID'] = rdf['product_ID'].astype('int64')
    printinfo(pdf)
    printinfo(rdf)
    pdf.to_csv(r'csv\product.csv', index=None, header=True)
    rdf.to_csv(r'csv\review.csv', index=None, header=True)


def datetimegenerator(start, end, n):
    start_u = start.value // 10 ** 9
    end_u = end.value // 10 ** 9

    return pd.DatetimeIndex((10 ** 9 * np.random.randint(start_u, end_u, n, dtype=np.int64)).view('M8[ns]'))


def idgenerator(size):
    return ''.join(random.choice(string.digits) for _ in range(size))


def pwdgenerator():
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32))


def printinfo(df):
    print(len(df))
    print(list(df.columns))
    print(df.dtypes)


if __name__ == "__main__":
    print('---Buyer and Seller DF---')
    buydf = combineuser(r'csv/UserInfo/Buyer', False)
    selldf = combineuser(r'csv/UserInfo/Seller', True)
    print('---Clean data info---')
    print(':::User:::')
    userdf = createuser(buydf, selldf)
    print(':::Metadata:::')
    cleanmetadf = combinemetadata(30000)
    print(':::Review:::')
    cleanreviewdf = combinereview(cleanmetadf, userdf, 100000)


    print('---CHANGE TYPE---')
    printinfo(cleanmetadf)
    printinfo(cleanreviewdf)
    joinProdRev(cleanmetadf, cleanreviewdf)

    #productcsv = cleanmetadf.to_csv(r'csv\product.csv', index=None, header=True)
    #review_csv = cleanreviewdf.to_csv(r'csv\review.csv', index=None, header=True)
    #prodrevcsv = prodrev_df.to_csv(r'csv\prodrev.csv', index=None, header=True)
    user_csv = userdf.to_csv(r'csv\user.csv', index=None, header=True)

