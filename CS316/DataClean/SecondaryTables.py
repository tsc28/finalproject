import pandas as pd
import numpy as np
import random
import string


def create_sells(user_df, product_df):
    productid_df = product_df['product_ID'].to_frame()
    seller_df = user_df.loc[(user_df['user_type'] == 1)]
    seller_df = seller_df.sample(n=productid_df.shape[0], replace=True)
    seller_df = seller_df['user_ID'].to_frame().reset_index(drop=True)

    sells_df = pd.concat([productid_df, seller_df], axis=1)
    printinfo(sells_df)
    return sells_df


def cartid(size):
    datac = np.array([idgenerator(9) for i in range(size)])
    cartid_df = pd.DataFrame(datac, columns=['cart_ID'])
    return cartid_df

def create_cart(user_df):
    buyer_df = user_df.loc[(user_df['user_type'] == 0)]
    cartidactive = cartid(buyer_df.shape[0])
    cartidinactive = cartid(2500)

    userid_df = buyer_df['user_ID'].to_frame()
    useridhalf_df = userid_df.head(2500)
    #userfinal_df = pd.concat([userid_df, useridhalf_df], axis=0).reset_index(drop=True)

    cartactive_df = pd.concat([cartidactive, userid_df], axis=1)
    cartinactive_df = pd.concat([cartidinactive, useridhalf_df], axis=1)

    cartactive_df['cart_status'] = 1
    cartinactive_df['cart_status'] = 0

    cart_df = pd.concat([cartactive_df, cartinactive_df], axis=0)
    cart_df = cart_df.drop_duplicates(subset=['cart_ID'], keep='first')
    cart_df['cart_ID'] = cart_df['cart_ID'].astype('int64')

    printinfo(cart_df)
    return cart_df


def create_orders(product_df, cart_df):
    sample_size = 20000
    cartuser_df = cart_df.sample(n=sample_size, replace=True)
    cartuser_df = cartuser_df[['cart_ID','user_ID']].reset_index(drop=True)
    productid_df = product_df.sample(n=sample_size, replace=True)
    productid_df = productid_df['product_ID'].to_frame().reset_index(drop=True)

    datao = np.array([idgenerator(9) for i in range(sample_size)])
    orderid_df = pd.DataFrame(datao, columns=['item_order_ID'])
    orderid_df['item_order_ID'] = orderid_df['item_order_ID'].astype('int64')

    order_df = pd.concat([orderid_df, cartuser_df, productid_df], axis=1)

    np.random.seed(40)
    order_df['quantity'] = np.random.randint(1, 10, order_df.shape[0])

    printinfo(order_df)
    return order_df


def create_purchases(product_df, cart_df, orders_df):
    cart_inactive = cart_df.loc[(cart_df['cart_status'] == 0)]
    cart_order_df = pd.merge(cart_inactive, orders_df, on=['cart_ID', 'user_ID'], how='inner')
    cart_order_purchases_df = pd.merge(cart_order_df, product_df, on='product_ID', how='inner')
    cart_order_purchases_df['amount'] = cart_order_purchases_df['price'] * cart_order_purchases_df['quantity']

    cart_order_purchases_df = cart_order_purchases_df[['cart_ID', 'user_ID', 'amount']]
    purchases_df = cart_order_purchases_df.groupby(['cart_ID', 'user_ID']).sum().reset_index()
    purchases_df = purchases_df[['cart_ID', 'user_ID', 'amount']]

    start = pd.to_datetime('2018-01-01')
    end = pd.to_datetime('2020-01-01')
    purchases_df['date_time'] = datetimegenerator(start, end, purchases_df.shape[0])

    printinfo(purchases_df)
    return purchases_df


def datetimegenerator(start, end, n):
    start_u = start.value//10**9
    end_u = end.value//10**9

    return pd.DatetimeIndex((10**9*np.random.randint(start_u, end_u, n, dtype=np.int64)).view('M8[ns]'))


def idgenerator(size):
    return ''.join(random.choice(string.digits) for _ in range(size))


def printinfo(df):
    print(len(df))
    print(list(df.columns))
    print(df.dtypes)


if __name__ == "__main__":
    user_df = pd.read_csv(r'csv/user.csv')
    product_df = pd.read_csv(r'csv/product.csv')
    review_df = pd.read_csv(r'csv/review.csv')

    sells_df = create_sells(user_df, product_df)
    sells_csv = sells_df.to_csv(r'csv\sells.csv', index=None, header=True)

    cart_df = create_cart(user_df)
    cart_csv = cart_df.to_csv(r'csv\cart.csv', index=None, header=True)

    orders_df = create_orders(product_df, cart_df)
    orders_csv = orders_df.to_csv(r'csv\orders.csv', index=None, header=True)

    purchases_df = create_purchases(product_df, cart_df, orders_df)
    purchases_csv = purchases_df.to_csv(r'csv\purchases.csv', index=None, header=True)