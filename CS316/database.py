#File for all general database functions, makes initial tables - Lydia
from flask import Flask, render_template, url_for, request
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
import random

#create a connection to database
def create_connection(db):
    conn = None
    try:
        conn = sqlite3.connect(db)
    except Error as e:
        print(e)
    return conn


#create table and link to database
def create_table(conn, create_table_query):
    try:
        c = conn.cursor()
        c.execute(create_table_query)
    except Error as e:
        print(e)


# insert() inserts any value into any table
# table = table to be inserted into, value = new value to be inserted into table
def insert(conn, table, value):
    sql = """INSERT INTO {} VALUES {}""".format(table, value)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return cur.lastrowid


def initialize_all_tables(conn):
    create_user_table = """CREATE TABLE User
                        (user_ID INTEGER NOT NULL PRIMARY KEY,
                        username TEXT NOT NULL UNIQUE,
                        name TEXT NOT NULL,
                        email TEXT NOT NULL,
                        password TEXT NOT NULL,
                        address text not null,
                        balance REAL NOT NULL,
                        user_type TEXT NOT NULL);"""

    create_products_table = """CREATE TABLE Products
                        (product_ID INTEGER NOT NULL PRIMARY KEY,
                        name TEXT NOT NULL,
                        brand TEXT NOT NULL,
                        price REAL NOT NULL,
                        pictures TEXT,
                        description TEXT,
                        inventory INTEGER NOT NULL);"""

    create_category_table = """CREATE TABLE Category
                        (category_name TEXT NOT NULL PRIMARY KEY);"""

    #cart_status boolean status: 0 = not active (previously purchased), 1 = active (cart currently being used by a user)
    create_cart_table = """CREATE TABLE Cart
                        (cart_ID INTEGER NOT NULL,
                        user_ID INTEGER NOT NULL,
                        cart_status INTEGER NOT NULL,
                        PRIMARY KEY(cart_ID),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID));"""

    create_review_table = """CREATE TABLE ReviewsRatings
                        (user_ID INTEGER NOT NULL,
                        product_ID INTEGER NOT NULL,
                        date_time NOT NULL default current_timestamp,
                        rating INTEGER NOT NULL,
                        review TEXT,
                        PRIMARY KEY(user_ID, product_ID, date_time),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(product_ID) REFERENCES Product(product_ID));"""

    create_address_table = """CREATE TABLE Address
                        (address_ID INTEGER NOT NULL PRIMARY KEY,
                        address TEXT NOT NULL,
                        city TEXT NOT NULL,
                        state TEXT NOT NULL,
                        zipcode INTEGER NOT NULL);"""

    create_sells_table = """CREATE TABLE Sells
                        (product_ID INTEGER NOT NULL PRIMARY KEY,
                        user_ID INTEGER NOT NULL,
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID));"""

    create_incategory_table = """CREATE TABLE InCategory
                        (product_ID INTEGER NOT NULL,
                        category_ID TEXT NOT NULL,
                        PRIMARY KEY(product_ID, category_ID),
                        FOREIGN KEY(product_ID) REFERENCES Product(product_ID),
                        FOREIGN KEY(category_ID) REFERENCES Category(category_name));"""

    #orderID used to simplify queries instead of using two primary keys
    create_orders_table = """CREATE TABLE Orders
                        (item_order_ID INTEGER NOT NULL,
                        cart_ID INTEGER NOT NULL,
                        product_ID INTEGER NOT NULL,
                        user_ID INTEGER NOT NULL,
                        quantity INTEGER NOT NULL,
                        PRIMARY KEY(item_order_ID),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(cart_ID) REFERENCES Cart(Cart_ID),
                        FOREIGN KEY(product_ID) REFERENCES Products(product_ID));"""

    create_purchases_table = """CREATE TABLE Purchases
                        (user_ID INTEGER NOT NULL,
                        cart_ID INTEGER NOT NULL,
                        amount REAL NOT NULL,
                        date_time NOT NULL default current_timestamp,
                        ship_address TEXT NOT NULL,
                        PRIMARY KEY(cart_ID),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(cart_ID) REFERENCES Cart(Cart_ID));"""

    create_hasAddress_table = """CREATE TABLE hasAddress
                        (user_ID INTEGER NOT NULL,
                        address_ID INTEGER NOT NULL,
                        PRIMARY KEY(user_ID, address_ID),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(address_ID) REFERENCES Address(address_ID));"""

    create_table(conn, create_user_table)
    create_table(conn, create_products_table)
    create_table(conn, create_category_table)
    create_table(conn, create_cart_table)
    create_table(conn, create_review_table)
    create_table(conn, create_address_table)
    create_table(conn, create_sells_table)
    create_table(conn, create_incategory_table)
    create_table(conn, create_orders_table)
    create_table(conn, create_purchases_table)
    create_table(conn, create_hasAddress_table)


#preloads below data into database
def preload_data(conn):
    user1 = (1, 'll123','Lydia Lin', 'lydia@gmail.com', 'password', '123 add rd', 5000.00, 'buyer')
    user1 = insert(conn, 'User', user1)
    user2 = (2, 'ps123','Patrick Star', 'pstar@gmail.com', 'passwd', '123 add rd',1000.00, 'buyer')
    user2 = insert(conn, 'User', user2)
    user3 = (3, 'sb123','Spongebob Squarepants', 'square@gmail.com', 'passwd', '123 add rd',4000.00, 'buyer')
    user3 = insert(conn, 'User', user3)

    cart1 = insert(conn, 'Cart(user_id, cart_status)', (user1, 1))
    cart2 = insert(conn, 'Cart(user_id, cart_status)', (user2, 1))
    cart3 =insert(conn, 'Cart(user_id, cart_status)', (user3, 1))

    category1 = insert(conn, 'Category(category_name)',  "('Pets')")
    category2 = insert(conn, 'Category', "('Outdoors')")
    category3 = insert(conn, 'Category', "('Hardware')")

    product1 = ('Dog Food', 'Petsmart', 10.50, 'pic.png', 'great for dogs', 20)
    product1  = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product1)
    product2 = ('Cat Food', 'Petsmart', 11.50, 'pic.png', 'great for cats', 20)
    product2 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product2)
    product3 = ('Human Food', 'Petsmart', 12.50, 'pic.png', 'great for people', 20)
    product3 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product3)
    product4 = ('Kayak', 'Outdoors Store', 50.50, 'pic.png', 'little floating boat', 10)
    product4 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product4)
    product5 = ('Hammer', 'Hardware Store', 10.00, 'pic.png', 'hard boi', 50)
    product5 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product5)
    product6 = ('Screwdriver', 'Hardware Store', 10.50, 'pic.png', 'great for driving screws', 20)
    product6 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product6)
    product7 = ('Super Squeaker Cat Toy', 'Petsmart', 11.50, 'pic.png', 'great for cats', 20)
    product7 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product7)
    product8 = ('Bookshelf', 'Walmart', 60.50, 'pic.png', 'great for books', 20)
    product8 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product8)
    product9 = ('Fishing rod', 'Outdoors Store', 20.50, 'pic.png', 'great for catching the fishies', 10)
    product9 = insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product9)
    product10 = ('Flashlight', 'Hardware Store', 10.00, 'pic.png', 'bright boi', 50)
    product10= insert(conn, 'Products(name, brand, price, pictures, description, inventory)', product10)

    insert(conn, 'InCategory', (product1, category1))
    insert(conn, 'InCategory', (product2, category1))
    insert(conn, 'InCategory', (product3, category1))
    insert(conn, 'InCategory', (product4, category2))
    insert(conn, 'InCategory', (product5, category3))
    insert(conn, 'InCategory', (product6, category3))
    insert(conn, 'InCategory', (product7, category1))
    insert(conn, 'InCategory', (product8, category3))
    insert(conn, 'InCategory', (product9, category2))
    insert(conn, 'InCategory', (product10, category3))

    order1 = (1, 1, 1, 1, 1)
    insert(conn, 'Orders', order1)
    order2 = (2, 1, 2, 1, 5)
    insert(conn, 'Orders', order2)
    order3 = (3, 1, 3, 1, 10)
    insert(conn, 'Orders', order3)
    order4 = (4, 1, 4, 1, 10)
    insert(conn, 'Orders', order4)

    for cid in range(4,23):
        uid = random.randint(1, 3)
        pid = random.randint(1, 10)

        cart = (uid, 0)
        insert(conn, 'Cart(user_id, cart_status)', cart)

        order = (cid, pid, uid, pid)
        insert(conn, 'Orders(cart_ID, product_ID, user_ID, quantity)', order)

        purchase = (uid, cid, pid*1.00, "123 ToomuchData Way, Help Me, NC 27708")
        insert(conn, 'Purchases(user_ID, cart_ID, amount, ship_address)', purchase)


#initializes entire database and preloaded values
def initialize_db(file):
    initiated = False
    if os.path.exists(file):
        initiated = True

    if not initiated:
        # creates database local file
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        database = os.path.join(THIS_FOLDER, file)
    else:
        print("Database already initialized")

    conn = create_connection(file)
    initialize_all_tables(conn)
    preload_data(conn)


#use to reset database tables when debugging/adding data
def reset(file):
    conn = create_connection(file)
    cur = conn.cursor()
    tables = list(cur.execute("""SELECT name from sqlite_master WHERE type is 'table'"""))
    cur.executescript(';'.join(["drop table if exists %s" %i for i in tables]))


if __name__ == "__main__":
    #reset('mini_amazon.db')
    initialize_db('mini_amazon.db')
