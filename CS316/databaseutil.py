# create a connection to database
from flask import Flask, render_template, url_for, request, redirect, session, Markup
import sqlite3
from sqlite3 import Error
import string
import random
import os
from flask import g

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
database = os.path.join(THIS_FOLDER, 'big_amazon.db')


def logged_in():
    if (not session.get('logged_in') or session['logged_in'] != True or not session.get('user_ID')):
        return False

    user_ID = session["user_ID"]
    conn = create_connection(database)

    sql = """ SELECT *
            FROM User
            WHERE user_ID= ?;"""
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql, (user_ID,))
    rows = cur.fetchall()

    if len(rows) == 0:
        return False

    cur.close()
    return True


def create_connection(database):
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(database)
    return db


# create table and link to database
def create_table(conn, create_table_query):
    try:
        c = conn.cursor()
        c.execute(create_table_query)
    except Error as e:
        print(e)


# insert() inserts any value into any table
# table = table to be inserted into, value = new value to be inserted into table
def insert(conn, sql, value):
    cur = conn.cursor()
    cur.execute(sql, (value))
    conn.commit()
    return cur.lastrowid


#use to reset database tables when debugging/adding data
def reset(file):
    conn = create_connection(file)
    cur = conn.cursor()
    tables = list(cur.execute("""SELECT name from sqlite_master WHERE type is 'table'"""))
    cur.executescript(';'.join(["drop table if exists %s" %i for i in tables]))
    print('Database reset')

#generate random alphanumeric ID, i.e. user_ID and Product_ID
def idgenerator(size):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(size))