from flask import Flask, render_template, url_for, request, redirect, session, flash
import sys
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os, time
from databaseutil import *
import hashlib


THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
database = os.path.join(THIS_FOLDER, 'big_amazon.db') #'mini_amazon.db')

app = Flask(__name__)

#add a review inputs are product ID and username, get info from session and find a way to pass productID
#presumably add review button will be attached in purchases and the item page
#extra: insitute upvotes of reviews, hit button equals update 'helpful'
#get review

def review_redirect():

    if request.method == 'POST':
        product_ID = request.form.get('product_ID')

    return render_template('review.html', product_ID=product_ID)


#check if user is a buyer, limit rating from 1 to 5
def user_add_review():
    conn = create_connection(database)
    username = session['username']
    user_ID = get_user(username, conn)

    if request.method == 'POST':
        try:
            summary = request.form.get("summary")
            star = request.form.get("star")
            rating = get_rating(star)
            review_text = request.form.get("review")
            product_ID = request.form.get("product_ID")
            date_time = datetime.now().replace(microsecond=0)

            review = (user_ID, product_ID, date_time, username, rating, review_text, summary, 0)
            sql = """INSERT INTO ReviewsRatings VALUES (?,?,?,?,?,?,?,?);"""
            cur = conn.cursor()
            cur.execute(sql, review)
            conn.commit()
            cur.close()
        except Error as e:
            conn.rollback()
            print(e, file=sys.stderr)

    return render_template('reviewsuccess.html')


def generate_reviews():
    product_ID = 238559804
    rows = []
    try:
        # creates database and connects
        conn = create_connection(database)

        sql = """SELECT * FROM ReviewsRatings WHERE ReviewsRatings.product_ID = ?;"""

        # returns partial match on name, brand, or description
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (product_ID,))
        rows = cur.fetchall()
        conn.commit()
        cur.close()
    except:
        conn.rollback()
        rows = []
    finally:
        return render_template('reviewtest.html', rows=rows, product_ID=product_ID)


def get_user(username, conn):
    sql = """SELECT user_ID FROM User WHERE username = ?;"""
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql, (username,))
    rows = cur.fetchall()
    user_ID = rows[0]["user_ID"]
    return user_ID


def get_rating(star):
    if star == 'star1':
        return 1
    elif star == 'star2':
        return 2
    elif star == 'star3':
        return 3
    elif star == 'star4':
        return 4
    else:
        return 5
