from flask import Flask, render_template, url_for, request, redirect, session, Markup
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
from databaseutil import *
from app import *

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
database = os.path.join(THIS_FOLDER, 'big_amazon.db')

no_image = "\static\images\pic.jpg"

#runs function for cart - Lydia
def my_cart(*args):
    if not logged_in():
        return redirect(url_for('login'))

    user_ID = session["user_ID"]

    cart_ID = -1
    total_cost = 0
    msg = ""

    try:
        conn = create_connection(database)

        sql = """ SELECT C.cart_ID, O.item_order_id
                FROM Cart C NATURAL JOIN Orders O
                WHERE cart_status = 1
                AND user_ID= ?;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (user_ID,))
        rows = cur.fetchall()

        if len(rows) > 0:
            cart_ID = rows[0]["cart_ID"]

            sql = """SELECT ROUND(SUM(P.price*O.quantity),2)
                    FROM Orders O NATURAL JOIN Products P
                    WHERE cart_ID = ?;"""
            cur = conn.cursor()
            cur.execute(sql, (cart_ID,))
            total_cost = cur.fetchone()[0]

            sql = """SELECT O.*, substr(P.name, 1, 50) as name, P.price, P.inventory,
                        (U.first_name || " " || U.last_name) AS seller,
                        ROUND((O.quantity*P.price),2) AS cost,
                        CASE WHEN P.image = '[]' THEN '{}' 
                            ELSE CASE WHEN INSTR(P.image, " ") = 0 THEN TRIM(P.image, "[]', ")
                            ELSE TRIM(substr(P.image, 1, INSTR(P.image, " ")), "[]', ")END END AS image
                    FROM Orders O NATURAL JOIN Products P
                        JOIN Sells S ON S.product_ID = P.product_ID
                        JOIN User U ON U.user_ID = S.user_ID
                    WHERE cart_ID = ?;""".format(no_image)

            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (cart_ID,))
            rows = cur.fetchall()
            conn.commit()
            cur.close()
        else:
            rows = []
            msg = "Cart Empty"
    except Error as e:
        msg = e
        conn.rollback()

    for arg in args:
        msg = Markup(arg)

    return render_template('cart.html', cart_ID = cart_ID, total_cost = total_cost, rows=rows, msg = msg)


#updates the quanitity of an item already in the cart when update button is clicked - Lydia
def my_update_item():
    if not logged_in():
        return redirect(url_for('login'))

    msg = ""
    if request.method == "POST":
        try:
            quantity = int(request.form["quantity"])
            item_order_ID = request.form["item_order_ID"]

            conn = create_connection(database)

            if quantity <= 0:
                msg = "Please enter a quantity greater than 0"
                return my_cart(msg)

            sql = """SELECT inventory, name
                    FROM Products NATURAL JOIN Orders
                    WHERE item_order_ID = ?;"""
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (item_order_ID,))
            rows = cur.fetchall()
            inventory = rows[0][0]

            if quantity > inventory:
                msg = "Insufficient number of copies of {}, please buy from other sellers.".format(rows[0][1])
                return my_cart(msg)

            sql = """UPDATE Orders
                    SET quantity = ?
                    WHERE item_order_ID = ?;"""
            cur = conn.cursor()
            cur.execute(sql, (quantity, item_order_ID))
            conn.commit()
            cur.close()
            msg = "Updated!"
        except Error as e:
            conn.rollback()
            msg = e

    return my_cart(msg)


#removes a corresponding item from cart when delete button is clicked - Lydia
def my_remove_item():
    if not logged_in():
        return redirect(url_for('login'))

    msg=""
    if request.method == "POST":
        try:
            item_order_ID = request.form["item_order_ID"]

            conn = create_connection(database)
            sql = """DELETE FROM Orders
                    WHERE item_order_ID = ?;"""
            cur = conn.cursor()
            cur.execute(sql, (item_order_ID,))
            conn.commit()
            cur.close()
            msg = "Item deleted"
        except Error as e:
            msg = e
            conn.rollback()

        return my_cart(msg)


#purchases all items in cart when purchase button is clicked and deducts cart cost from user balance - Lydia
def my_purchase_cart():
    if not logged_in():
        return redirect(url_for('login'))

    if request.method == "POST":
        user_ID = session["user_ID"]
        msg = ""

        cart_ID = request.form["cart_ID"]
        total_cost = float(request.form["total_cost"])

        if total_cost == 0:
            msg = "Your cart is empty. Please add item to cart to make purchase."
            return my_cart(msg)

        try:
            conn = create_connection(database)

            sql = """SELECT balance, user_ID
                    FROM User
                    WHERE user_ID = ?;"""
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (user_ID,))
            rows = cur.fetchall()
            if len(rows) > 0:
                user_balance = rows[0]["balance"]

                sql = '''SELECT *, S.user_ID AS seller
                        FROM Orders O NATURAL JOIN Products P 
                            JOIN Sells S ON S.product_ID = P.product_ID 
                        WHERE cart_ID = ?;'''
                conn.row_factory = sqlite3.Row
                cur = conn.cursor()
                cur.execute(sql, (cart_ID,))
                order_rows = cur.fetchall()

                suff_quant = True
                for order_index in range (0, len(order_rows)):
                    if order_rows[order_index]['quantity'] > order_rows[order_index]['inventory']:
                        msg = msg + """Insufficient number of copies of '{}', please buy from other sellers.<br>""".format(order_rows[order_index]['name'])
                        suff_quant = False
                if not suff_quant:
                    return my_cart(msg)

                if user_balance > total_cost:
                    for order_index in range(0, len(order_rows)):
                        quantity = order_rows[order_index]['quantity']
                        pid = order_rows[order_index]['product_ID']
                        price = order_rows[order_index]['price']*quantity
                        sid = order_rows[order_index]['seller']

                        sql = """UPDATE Products
                                SET inventory = inventory - ?
                                WHERE product_ID = ?;"""
                        cur = conn.cursor()
                        cur.execute(sql, (quantity, pid))

                        sql = """UPDATE User
                                SET balance = balance + ?
                                WHERE user_type = 1 and user_ID = ?;"""
                        cur = conn.cursor()
                        cur.execute(sql, (price, sid))

                    sql = """UPDATE Cart
                            SET cart_status = 0
                            WHERE cart_ID = ?;"""
                    cur = conn.cursor()
                    cur.execute(sql, (cart_ID,))

                    sql = """UPDATE User
                            SET balance = balance - ?
                            WHERE user_type = 0 and user_ID = ?;"""
                    cur = conn.cursor()
                    cur.execute(sql, (total_cost, user_ID))

                    purchase = (cart_ID, user_ID, total_cost)
                    sql = """INSERT INTO Purchases(cart_ID, user_ID, amount) VALUES (?,?,?);"""
                    cur = conn.cursor()
                    cur.execute(sql, purchase)

                    check_and_create_cart(conn)

                    conn.commit()
                    cur.close()
                    msg = "Purchase successful!"
                else:
                    msg = "Not enough funds in balance to purchase cart"
        except Error as e:
            msg = e
            conn.rollback()

        return my_cart(msg)


def check_and_create_cart(conn):
    if not logged_in():
        return redirect(url_for('login'))

    msg = ""
    try:
        user_ID = session["user_ID"]

        sql = """ SELECT C.cart_ID 
                FROM Cart C  
                WHERE cart_status = 1
                AND user_ID= ?;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (user_ID,))
        rows = cur.fetchall()

        if len(rows) == 0:
            new_cart = (user_ID, 1)
            sql = """INSERT INTO Cart(user_id, cart_status) VALUES (?,?);"""
            cur = conn.cursor()
            cur.execute(sql, new_cart)
            conn.commit()
    except Error as e:
        conn.rollback()
        msg = e

    return my_cart(msg)


def my_add_to_cart():
    if not logged_in():
        return redirect(url_for('login'))

    user_ID = session["user_ID"]
    msg = ""

    if request.method == "POST":
        try:
            quantity = int(request.form["quantity"])
            product_ID = request.form["product_ID"]

            conn = create_connection(database)

            if quantity <= 0:
                msg = "Please enter a quantity greater than 0"
                return my_cart(msg)

            sql = """SELECT inventory, name
                    FROM Products
                    WHERE product_ID = ?;"""
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (product_ID,))
            rows = cur.fetchall()
            inventory = rows[0][0]

            if quantity > inventory:
                msg = "Insufficient number of copies of {}, please buy from other sellers.".format(rows[0][1])
                return my_cart(msg)

            sql = """ SELECT cart_ID
                    FROM Cart  
                    WHERE cart_status = 1
                    AND user_ID= ?;"""
            cur = conn.cursor()
            cur.execute(sql, (user_ID,))
            cart_ID = cur.fetchone()[0]

            #checks and merges items that are added with same product ID
            sql = """SELECT *
                    FROM Orders
                    WHERE cart_ID = ? and product_ID = ?;"""
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (cart_ID, product_ID))
            rows = cur.fetchall()

            if len(rows) > 0:
                new_quantity = rows[0]["quantity"] + quantity
                if new_quantity > inventory:
                    msg = "Insufficient number of copies of {} given current quantity in cart, please buy from other sellers.".format(rows[0][1])
                    return my_cart(msg)

                sql = """UPDATE Orders
                        SET quantity = ?
                        WHERE cart_ID = ? and product_ID = ?;"""
                cur = conn.cursor()
                cur.execute(sql, (new_quantity, cart_ID, product_ID))
            else:
                order = (cart_ID, product_ID, user_ID, quantity)
                sql = """INSERT INTO Orders(cart_ID, product_ID, user_ID, quantity) VALUES (?,?,?,?);"""
                cur = conn.cursor()
                cur.execute(sql, order)

            conn.commit()
            msg = "Added to cart!"
        except Error as e:
            conn.rollback()
            msg = e

    return my_cart(msg)



#Balance related functions
#displays the balance of the current logged in user
def my_balance(*args):
    if not logged_in():
        return redirect(url_for('login'))

    user_ID = session["user_ID"]

    balance = -1
    msg = ""
    try:
        conn = create_connection(database)
        sql = """SELECT ROUND(balance, 2)
                FROM User
                WHERE user_ID = ?;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (user_ID,))
        rows = cur.fetchall()
        balance = rows[0][0]
        conn.commit()
        cur.close()
    except Error as e:
        conn.rollback()
        msg = e

    for arg in args:
        msg = arg

    return render_template('balance.html', balance=balance, msg=msg)


def valid_amt(n):
    try:
        float(n)
        return True
    except ValueError:
        return False


#updates the balance of the current logged in user based on what is inputed into form
def my_update_balance():
    if not logged_in():
        return redirect(url_for('login'))

    msg = ""
    if request.method == "POST":
        user_ID = session["user_ID"]
        added_amt = request.form["added_amt"]

        if (not valid_amt(added_amt) or float(added_amt) < 0):
            msg = "try again"
        else:
            try:
                    conn = create_connection(database)
                    sql = """UPDATE User
                            SET balance = balance + ?
                            WHERE user_ID = ?;"""
                    cur = conn.cursor()
                    cur.execute(sql, (added_amt, user_ID))
                    conn.commit()
                    cur.close()
                    msg = "success!!!"
            except Error as e:
                conn.rollback()
                msg = e

        return my_balance(msg)



#runs function for cart - Lydia
def my_purchase_history():
    if not logged_in():
        return redirect(url_for('login'))

    user_ID = session["user_ID"]

    msg = ""
    rows = []

    try:
        conn = create_connection(database)

        sql = """ SELECT C.cart_ID, O.item_order_id
                FROM Cart C NATURAL JOIN Orders O 
                WHERE cart_status = 0
                AND user_ID= ?;"""
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (user_ID,))
        rows = cur.fetchall()

        if len(rows) > 0:
            sql = """SELECT O.*, substr(P.name, 1, 50) as name, P.price, P1.date_time, P.inventory,
                        (U.first_name || " " || U.last_name) AS seller,
                        ROUND((O.quantity*P.price),2) AS cost,
                        CASE WHEN P.image = '[]' THEN '{}' 
                            ELSE CASE WHEN INSTR(P.image, " ") = 0 THEN TRIM(P.image, "[]', ")
                            ELSE TRIM(substr(P.image, 1, INSTR(P.image, " ")), "[]', ")END END AS image
                    FROM (Orders O NATURAL JOIN Purchases P1 NATURAL JOIN Products P)
                        JOIN Sells S ON S.product_ID = P.product_ID
                        JOIN User U ON U.user_ID = S.user_ID
                    WHERE P1.user_ID = ?;""".format(no_image)

            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute(sql, (user_ID,))
            rows = cur.fetchall()
            conn.commit()
            cur.close()
        else:
            msg = "No purchases made yet"
    except Error as e:
        msg = e
        conn.rollback()

    return render_template('purchasehistory.html', rows=rows, msg = msg)


