from flask import Flask, render_template, url_for, request, redirect, session, flash
from flask_mail import Mail, Message
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
from database import *
from user_related import *
from search_related import *
from purchase_related import *
from review_related import *
from seller_related import *

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
database = os.path.join(THIS_FOLDER, 'big_amazon.db')

app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY', 'Optional default value')

#runs function for website main search page
@app.route('/')
def index(): #index
    return homepage()

#search function
@app.route('/searchresults', methods = ["POST"])
def search():
    return search_searchThis()

#item page (Wendy)
@app.route('/item', methods = ['GET'])
def showItem():
    return search_showItem()

#user page (user profile and purchasing history)
    #Tracey
import hashlib
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = "miniamazonCS316F20@gmail.com"
app.config['MAIL_PASSWORD'] = "Password!0"
mail = Mail(app)

@app.route('/user')
def checkStatus():
    return user_checkStatus()

@app.route('/user/<username>')
def user(username):
    return user_user(username)
#Tracey, sign-up page
@app.route('/signup', methods=['POST', 'GET'])
def signup():
    return user_signup()

@app.route('/login', methods = ['POST', 'GET'])
def login():
    return user_login()

@app.route("/logout")
def logout():
    return user_logout()

@app.route("/forgetPassword", methods = ['POST', 'GET'])
def forgetPassword():
    return user_forgetPassword()

@app.route("/resetPassword", methods = ['POST', 'GET'])
def resetPassword():
    return user_resetPassword()

@app.route("/editUser", methods = ['POST', 'GET'])
def editUser():
    return user_editUser()


#Cart functions-Lydia
#runs function for cart
@app.route('/cart')
def cart():
    return my_cart()

#updates the quanitity of an item already in the cart when update button is clicked
@app.route('/update_item', methods = ["POST"])
def update_item():
    return my_update_item()

#removes a corresponding item from cart when delete button is clicked
@app.route('/remove_item', methods = ["POST"])
def remove_item():
    return my_remove_item()

#purchases all items in cart when purchase button is clicked and deducts cart cost from user balance
@app.route('/purchase_cart', methods = ["POST"])
def purchase_cart():
    return my_purchase_cart()


#Balance related functions-Lydia
#displays the balance of the current logged in user
@app.route('/balance')
def balance():
    return my_balance()

#updates the balance of the current logged in user based on what is inputed into form
@app.route('/update_balance', methods = ["POST"])
def update_balance():
    return my_update_balance()


@app.route('/rgenerate')
def generatereviews():
    return generate_reviews()

@app.route('/review', methods = ["POST"])
def revdirect():
    return review_redirect()

@app.route('/addrev', methods = ["POST"])
def add_review():
    return user_add_review()


#not sure if this is correct but it should render seller page
@app.route('/seller')
def seller():
    return seller_item_list()

#not sure if this is correct but it should remove item from seller list
@app.route('/seller', methods = ["POST"])
def seller_delete_item():
    return seller_remove_item()

@app.route('/addModifyItem', methods = ["POST", "GET"])
def seller_addItem():
    return seller_add_item()

@app.route('/tradeHistory')
def seller_history():
    return trade_history()

# purchase history -Lydia
@app.route('/purchasehistory')
def purchase_history():
    return my_purchase_history()

# add to cart function -Lydia
@app.route('/add_to_cart', methods = ["POST"])
def add_to_cart():
    return my_add_to_cart()

#closes connection upon app termination -Lydia
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == "__main__":
    #app.secret_key = os.urandom(12)
    app.run(debug=True)#,host='0.0.0.0', port=8080)
