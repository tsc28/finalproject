import os
from databaseutil import *
from app import *

# import sqlite3
# import pandas as pd
#
# conn = sqlite3.connect('big_amazon.db')
# c = conn.cursor()
#
# read_clients = pd.read_csv(r'dataclean/csv/user.csv')
# read_clients.to_sql('USER', conn, if_exists='append',
#                     index=False)

def initialize_all_tables(conn):
    c = conn.cursor()

    c.execute("""CREATE TABLE Products
                        (product_ID INTEGER NOT NULL PRIMARY KEY,
                        name TEXT NOT NULL, 
                        brand TEXT NOT NULL, 
                        category TEXT NOT NULL, 
                        price MONEY NOT NULL,
                        feature TEXT,
                        image TEXT, 
                        description TEXT,
                        inventory SMALLINT NOT NULL);""")

    c.execute("""CREATE TABLE ReviewsRatings
                        (user_ID INTEGER NOT NULL,
                        product_ID INTEGER NOT NULL,
                        date_time DATETIME NOT NULL,
                        username TEXT,
                        rating SMALLINT NOT NULL, 
                        review TEXT,
                        summary TEXT,
                        vote SMALLINT DEFAULT 0,
                        PRIMARY KEY(user_ID, product_ID, date_time),
                        FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                        FOREIGN KEY(product_ID) REFERENCES Product(product_ID));""")

    #user_type 0 is buyer 1 is seller
    c.execute("""CREATE TABLE User
                            (user_ID INTEGER NOT NULL PRIMARY KEY,
                            username TEXT NOT NULL, 
                            first_name TEXT NOT NULL,
                            last_name TEXT NOT NULL,
                            email TEXT NOT NULL,
                            password TEXT NOT NULL,
                            balance MONEY NOT NULL,
                            user_type BIT NOT NULL,
                            street_address TEXT NOT NULL,
                            city TEXT NOT NULL,
                            postalcode TEXT NOT NULL,
                            state TEXT NOT NULL);""")

    #cart_status boolean status: 0 = not active (previously purchased), 1 = active (cart currently being used by a user)
    c.execute("""CREATE TABLE Cart
                        (cart_ID INTEGER NOT NULL,
                        user_ID INTEGER NOT NULL,
                        cart_status INTEGER NOT NULL,
                        PRIMARY KEY(cart_ID),
                        FOREIGN KEY(user_ID) REFERENCES USER(user_ID));""")

    c.execute("""CREATE TABLE Sells
                            (product_ID INTEGER NOT NULL PRIMARY KEY,
                            user_ID INTEGER NOT NULL,
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(product_ID) REFERENCES Products(product_ID));""")

    # orderID used to simplify queries instead of using two primary keys
    c.execute("""CREATE TABLE Orders
                            (item_order_ID INTEGER NOT NULL,
                            cart_ID INTEGER NOT NULL,
                            user_ID INTEGER NOT NULL,
                            product_ID INTEGER NOT NULL,
                            quantity INTEGER NOT NULL,
                            PRIMARY KEY(item_order_ID),
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(cart_ID) REFERENCES Cart(cart_ID),
                            FOREIGN KEY(product_ID) REFERENCES Products(product_ID));""")

    c.execute("""CREATE TABLE Purchases
                            (cart_ID INTEGER NOT NULL,
                            user_ID INTEGER NOT NULL,
                            amount REAL NOT NULL,
                            date_time NOT NULL default current_timestamp,
                            PRIMARY KEY(cart_ID),
                            FOREIGN KEY(user_ID) REFERENCES User(user_ID),
                            FOREIGN KEY(cart_ID) REFERENCES Cart(cart_ID));""")

    conn.commit()


# initializes entire database and preloaded values
def initialize_db(file):
    initiated = False
    if os.path.exists(file):
        initiated = True

    if not initiated:
        # creates database local file
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        database = os.path.join(THIS_FOLDER, file)
    else:
        print("Database already initialized")

    conn = create_connection(file)
    initialize_all_tables(conn)


if __name__ == "__main__":
    with app.app_context():
        reset('big_amazon.db')
        initialize_db('big_amazon.db')