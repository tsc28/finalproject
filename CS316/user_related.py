from flask import Flask, render_template, url_for, request, redirect, session, flash
from flask_mail import Mail, Message
import sys
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os, time
from databaseutil import *
from purchase_related import *
import hashlib, random

#to do: forget password, password_hash, editUserPage
#password_hashing: https://pythonprogramming.net/password-hashing-flask-tutorial/
#password+salt -> __.encode() -> hashlib.md5(__) -> __.hexdigest()
#all users currently have "password" as password, which is b305cadbb3bce54f3aa59c64fec00dea in hash (in User table)

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
#database = os.path.join(THIS_FOLDER, 'database.py')
database = os.path.join(THIS_FOLDER, 'big_amazon.db') #'mini_amazon.db')
salt = 'salt'

app = Flask(__name__)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = "miniamazonCS316@gmail.com"
app.config['MAIL_PASSWORD'] = "Password!0"
mail = Mail(app)

def user_checkStatus():
    if session.get('logged_in'):
        return redirect(url_for('user', username = session.get("username")))
    return redirect(url_for('login'))

def user_user(username):
    #user_ID, username, name, email, password, address, balance, user_type
    #username = string(username)
    uname = str(username)
    printData = None

    if session.get("username") == uname:
        conn = create_connection(database)
        #sql = '''SELECT user_ID,username,name,email,address FROM User where username = %s''',username #%d for numbers and %s for strings
        #print(sql, file=sys.stderr)
        cur = conn.cursor()
        sql = '''select user_ID from User WHERE username = ?;'''
        cur.execute(sql,(uname,))
        #cur.execute('''select user_ID from User WHERE username = "{}"'''.format(uname))
        conn.row_factory = sqlite3.Row
        row = cur.fetchall()
        userid = row[0][0]

# once purchases and orders tables are finalized, complete below:

        sql = """select Purchases.date_time, products.product_ID, products.name, products.price, Orders.quantity
                    from Orders, Purchases, products
                    where Purchases.user_ID = ? and Orders.cart_ID = Purchases.cart_ID and Orders.product_ID = products.product_ID;"""
        cur.execute(sql,(userid,))

        printData = cur.fetchall()
        sql = '''SELECT username,first_name, last_name, email, balance, street_address, city, postalcode,state FROM User where username = ?;'''
        cur.execute(sql, (uname,)) 
        userProf = cur.fetchall()
        return render_template('user.html', userid = userid, userProf = userProf, printData = printData)


    else:
        flash("No user ID found in session")
        return redirect(url_for("login"))

def user_signup():
    #user_ID, username, name, email, password, address, balance, user_type
    session['logged_in'] = False
    session.pop("username", None) #log out first (so there won't be multiple users logged in at once later)
    session.pop("user_type", None)
    session.pop("user_ID", None)

    error = None

    conn = create_connection(database)
    cur = conn.cursor()
    cur.execute("SELECT username FROM User")
    allUsernames = [item[0] for item in cur.fetchall()]
    cur.execute("select email from User")
    allEmails = [item[0] for item in cur.fetchall()]

    if request.method=='POST':
        #name = request.form['name']
        #email = request.form['email']
        #password = request.form['password']
        #address = request.form['address'] #new database and user table will have address column
        #username = request.form['username'] #new database will have this column
        firstName = request.form.get("firstName")
        lastName = request.form.get("lastName")
        email = str(request.form.get("email")).lower()
        password = request.form.get("password")
        username = request.form.get("username")
        streetAddress = request.form.get("streetAddress")
        city = request.form.get("city")
        zip = request.form.get("zip")
        state = request.form.get("state")
        role = str(request.form.get('role'))
        if role == "Buyer": #set buyer == 0 in table, or is it the other way around?
            role = 0
        elif role == "Seller":
            role = 1

        print(username, file=sys.stderr)
        print(type(username), file=sys.stderr)

        #if name == '' or email == '' or password == '' or role == '' or username == '' or address == '':
        #    error = 'Fields cannot be empty. Please try again.'
        #    return render_template('signup.html', role = role, error = error)

        if username in allUsernames:
            error = 'Username already in use. Please select another username'
            return render_template('signup.html', role = role, error = error)

        if email in allEmails:
            error = 'Email already in use. Please select another username'
            return render_template('signup.html', role = role, error = error)

        else:
            conn = create_connection(database)
            print(database, file = sys.stderr)
            cur = conn.cursor()
            pw = hashlib.md5((password+salt).encode())
            #print(username, file=sys.stderr)
            #print(type(username), file=sys.stderr)
            #sql = '''insert into User(username, first_name, last_name, email, password,
            #        balance, user_type, street_address, city, postalcode, state)
            #        values ("{}","{}","{}","{}","{}",{},{},"{}","{}","{}",
            #        "{}")'''.format(username, firstName, lastName, email, pw.hexdigest(), 0, role, streetAddress, city, zip, state)
            #print(sql, file=sys.stderr)
            #cur.execute(sql)
            newUser = (username, firstName, lastName, email, pw.hexdigest(), 0, role, streetAddress, city, zip, state)
            sql = '''insert into User(username, first_name, last_name, email, password,
                        balance, user_type, street_address, city, postalcode, state) values (?,?,?,?,?,?,?,?,?,?,?);'''
            cur.execute(sql, newUser)
            conn.commit()
            user_id = cur.lastrowid

            #insert(conn, '''User(username, first_name, last_name, email, password,
            #            balance, user_type, street_address, city, postalcode, state)''', newUser)

            session['logged_in'] = True
            session["username"] = username
            session["user_type"] = role
            session["user_ID"] = user_id

            check_and_create_cart(conn)

            return redirect(url_for('checkStatus'))
    else:
        return render_template('signup.html')

def user_editUser():
    error1 = None
    error2 = None

    if request.method == 'get':
        return render_template('editUser.html')

    else: #method == 'post':
        if session.get('logged_in'):# and request.form['submit'] == "Save Changes":
            uname = session.get("username")

            username = request.form.get("username")
            print(username, file=sys.stderr)
            print(type(username), file=sys.stderr)

            firstName = request.form.get("firstName")
            lastName = request.form.get("lastName")
            email = str(request.form.get("email")).lower()
            streetAddress = request.form.get("streetAddress")
            city = request.form.get("city")
            zip = request.form.get("zip")
            state = request.form.get("state")
            #lastName = request.form.get("lastName")
            pw = request.form.get("oldPassword")

            conn = create_connection(database)
            cur = conn.cursor()
            cur.execute("SELECT username FROM User")
            allUsernames = [item[0] for item in cur.fetchall()]

            cur.execute("select email from User")
            allEmails = [item[0] for item in cur.fetchall()]

            #cur.execute('''select user_ID from User WHERE username = "{}"'''.format(uname))
            #userid = cur.fetchall()

            cur = conn.cursor()
            sql = '''select * from User WHERE username = ?;'''
            cur.execute(sql, (uname,))
            conn.row_factory = sqlite3.Row
            row = cur.fetchall()
            userid = row[0][0]
            emailOld = row[0][4]
            userProf = row
            #if username == '' or name == '' or email == '' or address == '':
                #error1 = "Error: Fields cannot be empty. Please try again."
                #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)

            if request.form.get('submit') == "Save Changes":
                if username != uname and username in allUsernames:
                    error1 = 'Error: Username already in use. Please select another username'
                    #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)

                elif email != emailOld and email in allEmails:
                    error1 = 'Error: Email already in use. Please use another email'
                    #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)

                elif pw == '':
                    sql = '''UPDATE User SET username = ?, first_name = ?,last_name = ?,
                            email = ?, street_address = ?, city = ?, postalcode = ?, state = ?
                            WHERE user_ID = ?;'''
                    cur.execute(sql, (username, firstName, lastName, email, streetAddress, city, zip, state, userid,))
                    conn.commit()
                    session["username"] = username
                    flash("Your information has been updated")
                    #time.sleep(3)
                    #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)
                    return redirect(url_for('editUser'))

                elif pw != '':
                    #error1 = str(request.form.get('oldPassword'))
                    #error2 = type(username)
                    password = hashlib.md5((pw+salt).encode()).hexdigest()
                    newPassword = request.form.get('newPassword')
                    newPasswordHashed = hashlib.md5((newPassword+salt).encode()).hexdigest()
                    newPassword2 = request.form.get('newPassword2')

                    sql = '''select password from User WHERE username = ?;'''
                    cur.execute(sql,(uname,))
                    existingPW = str(cur.fetchall()[0][0])
                    print(existingPW, file=sys.stderr)
                    print(password, file=sys.stderr)

                    #error2 = pw
                    if password != existingPW: #if incorrect oldPassword
                        error2 = "Error: Incorrect old password. Please try again."
                      #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)

                    elif password == existingPW and newPassword != newPassword2: #correct oldPassword and correct newPW1 and newPW2
                        error2 = "Error: New passwords do not match. Please try again."
                      #return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)

                    elif password == existingPW and newPassword == newPassword2:
                        sql = '''UPDATE User SET username = ?, first_name = ?,last_name = ?,
                                email = ?, street_address = ?, city = ?, postalcode = ?, state = ?,
                                password = ? WHERE user_ID = ?;'''
                        cur.execute(sql,(username, firstName, lastName, email, streetAddress, city, zip, state, newPasswordHashed, userid,))
                        flash("Your information has been updated. Please re-log in")
                        session['logged_in'] = False
                        session.pop("username", None)
                        session.pop("user_type", None)
                        session.pop("user_ID", None)
                        conn.commit()
                        #time.sleep(3)
                        return homepage()
            return render_template('editUser.html', userProf = userProf, error1 = error1, error2 = error2)
        else:
            return redirect(url_for("login"))


def user_login():
    error = None
    conn = create_connection(database)
    cur = conn.cursor()
    sql = "SELECT (username, password) FROM User" #%d for numbers and %s for strings
    cur.execute('select username,password from User')
    lst = cur.fetchall()
    if request.method == 'POST':
        username = str(request.form['username'])
        pw = str(request.form['password'])+salt
        password = hashlib.md5(pw.encode()).hexdigest()
        if ((username, password) not in lst): #request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            session['logged_in'] = True #matches with logout's Set False
            session["username"] = username #matches with logout's pop

            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            cur.execute("""SELECT * from User WHERE username = ?""", (username,))
            rows = cur.fetchall()

            session["user_type"] = rows[0]['user_type']
            session["user_ID"] = rows[0]['user_ID']
            #lst = session
            return redirect(url_for("index"))#, username = username))
    return render_template('login.html', error=error)

def user_logout():
    session['logged_in'] = False
    session.pop("username", None)
    session.pop("user_type", None)
    session.pop("user_ID", None)
    return redirect(url_for("login"))

def get_random_string(length=24,allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    return ''.join(random.choice(allowed_chars) for i in range(length))

def user_forgetPassword():
    error = None
    conn = create_connection(database)
    cur = conn.cursor()
    #cur.execute("SELECT username FROM User")
    #allUsernames = [item[0] for item in cur.fetchall()]
    cur.execute("select email from User")
    allEmails = [item[0] for item in cur.fetchall()]

    if request.method == 'get':
        return render_template("forgetPassword.html")
    else:
        email = str(request.form.get('email'))
        email = email.lower()
        #error = "uh wtf"
        #error = type(email)
        #if request.form.get("submit") == "Send":
            #error = ("got email")
        if email == '':
            error = None
            #return render_template("forgetPassword.html", error = error)
        elif email not in allEmails:
            error = "Error: No account associated with this email."
            #return render_template("forgetPassword.html", error = error)
        else:
            #flash("I'm here")
            randomKey = get_random_string()
            #flash(randomKey)
            print(randomKey, file=sys.stderr)
            session[email] = randomKey
            print(session, file = sys.stderr)

            msg = Message()
            msg.subject = "Password Reset from Mini Amazon"
            msg.recipients = [email]
            msg.sender = 'miniamazonCS316@gmail.com'
            msg.body = 'Please copy this secret key to Reset Password within the next 24 hours: '+randomKey
            mail.send(msg)
            flash("Sent. Check your inbox for reset instructions.")
            #return render_template("forgetPassword.html", error = error)
        return render_template("forgetPassword.html", error = error)
    return render_template("forgetPassword.html", error = error)

def user_resetPassword():
    error = None
    if request.method == 'get':
        return render_template("resetPassword.html")
    else:
        email = str(request.form.get('email'))
        email = email.lower()
        #email = str(request.form['email'])
        skey = str(request.form.get('skey'))
        newPassword = str(request.form.get('newPassword'))
        newPasswordHashed = hashlib.md5((newPassword+salt).encode()).hexdigest()
        newPassword2 = request.form.get('newPassword2')
        #if request.form.get('submit') == "Confirm":
        if newPassword != newPassword2:
            error = "Passwords do not match"
        elif session.get(email) is None:
            error = "Please retrieve a secret key for this email first by 'Forget Password'"
        elif session[email] is None or skey not in session[email]:
            error = "Secret key is not recognized"
        elif skey == session[email] and newPassword == newPassword2:
            conn = create_connection(database)
            cur = conn.cursor()
            sql = '''UPDATE User SET password = ? WHERE email = ?;'''
            cur.execute(sql,(newPasswordHashed, email,))
            conn.commit()
            session[email] = None
            print(session, file=sys.stderr)
            flash("Your password has been reset. Please re-log in")
            return redirect(url_for("login"))
        return render_template("resetPassword.html", error = error)



    #not working yet
    #pass
