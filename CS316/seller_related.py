from flask import Flask, render_template, url_for, request, redirect, session, flash
import sqlite3
from sqlite3 import Error
from datetime import datetime
import os
from database import *
from databaseutil import *
from purchase_related import *
from app import *

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

database = os.path.join(THIS_FOLDER, 'big_amazon.db')

app = Flask(__name__)

#creates the sellers list based on userID
def seller_item_list():
    # uname = "hglindejr"
    if session.get("username"):
        uname = session.get("username")
        database = r"./big_amazon.db"
        conn = create_connection(database)
        cur = conn.cursor()
        sql = '''select user_ID from User WHERE username = ?;'''
        cur.execute(sql, (uname,))
        conn.row_factory = sqlite3.Row
        userid = cur.fetchall()[0][0]

        sql = """select p.image, p.name, p.product_ID, p.price, p.inventory
                        from Products p, Sells s
                        where s.user_ID = ? and p.product_ID = s.product_ID;"""
        cur.execute(sql, (userid,))
        rows = cur.fetchall()


        sql = '''SELECT username,first_name, last_name, email, balance, street_address FROM User WHERE username = ?;'''
        cur.execute(sql, (uname,))
        userProf = cur.fetchall()
        return render_template('seller.html', userProf=userProf, rows=rows)


    else:
        flash("No user ID found in session")
        return redirect(url_for("login"))


#deletes an item from the seller's list
def seller_remove_item():

    if request.method == "POST":
        try:
            productID = request.form["product_ID"]
            database = r"./big_amazon.db"
            conn = create_connection(database)
            sql = """DELETE FROM Sells
                    WHERE product_ID = ?;"""
            cur = conn.cursor()
            cur.execute(sql, (productID,))
            conn.commit()

        except Error:
            conn.rollback()
        finally:
            conn.close()

        return seller_item_list()

def seller_add_item():
    database = r"./big_amazon.db"
    conn = create_connection(database)
    cur = conn.cursor()
    cur.execute("SELECT product_ID FROM Products")
    allProducts = [item[0] for item in cur.fetchall()]

    if not session.get('logged_in'):
        return redirect(url_for('login'))

    if request.method=='POST':

        productID = request.form.get("product_ID")
        productName = request.form.get("product_Name")
        brand = request.form.get("brand")
        category = request.form.get("category")
        price = request.form.get("price")
        feature = request.form.get("feature")
        image = request.form.get("image")
        description = request.form.get("description")
        inventory = request.form.get("inventory")
        if productID in allProducts:
            conn = create_connection(database)
            print(database, file=sys.stderr)
            cur = conn.cursor()

            # might need to add values (????????) to the end of this query like on the insert statement
            newProduct = (productName, brand, category, price, feature, image, description, inventory, productID)
            sql = '''UPDATE Products set name = ?, brand, category = ?, price = ?, feature = ?, image = ?,
            description = ?, inventory = ?  where = product_ID = ?;'''
            cur.execute(sql, newProduct)
            conn.commit()
            conn.close()
            return render_template('addModifyItem.html')
        else:
            conn = create_connection(database)
            print(database, file = sys.stderr)
            cur = conn.cursor()

            newProduct = (productID, productName, brand, category, price, feature, image, description, inventory)
            sql = '''insert into Products(product_ID, name, brand, category, price,
                        feature, image , description, inventory) values (?,?,?,?,?,?,?,?,?);'''
            cur.execute(sql, newProduct)
            conn.commit()
            conn.close()
            return render_template('addModifyItem.html')
    else:
        conn.close()
        return render_template("addModifyItem.html")


# creates trade history page
def trade_history():
    uname = session["username"]
    # uname = "tkubacek7n"
    msg = ""
    rows = []

    try:
        database = r"./big_amazon.db"
        conn = create_connection(database)
        cur = conn.cursor()
        sql = '''select user_ID from User WHERE username = ?;'''
        cur.execute(sql, (uname,))
        conn.row_factory = sqlite3.Row
        userid = cur.fetchall()[0][0]

#need to get a list of things the seller sells
        sql = """SELECT P.image, substr(P.name, 1, 50) as name, P.product_ID,
                            (U.first_name || " " || U.last_name) AS buyer, P.price, O.quantity,
                            ROUND((O.quantity*P.price),2) AS cost, P.inventory, P1.date_time
                        FROM (Orders O NATURAL JOIN Purchases P1 NATURAL JOIN Products P)
                        JOIN User U ON U.user_ID = P1.user_ID
                        Join Sells S on S.product_ID = P.product_ID
                        where S.user_ID = ?;"""

        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql, (userid,))
        rows = cur.fetchall()
        conn.commit()
        return render_template('tradeHistory.html', rows=rows)


        if len(rows) > 0:
            conn.close()
            return render_template('tradeHistory.html', rows=rows)
        else:
            msg = "You have not sold anything yet."
    # except Error as e:
    #     # msg = "hello"
    #     conn.rollback()
    finally:
        conn.close()

    return render_template('tradeHistory.html', rows=rows, msg=msg)


# sql = """ SELECT C.cart_ID, O.item_order_id
#                FROM Cart C NATURAL JOIN Orders O
#                WHERE cart_status = 0
#                AND {row['product_ID']} =  ?;"""
# conn.row_factory = sqlite3.Row
# cur = conn.cursor()
# cur.execute(sql, (user_ID,))
# rows = cur.fetchall()
